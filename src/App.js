import React, {Component} from 'react';
import './App.css';
import UserOutput from './UserOutput/UserOutput';
import UserInput from './UserInput/UserInput';

class App extends Component {

  state = {
    username : "Kemozera"
  }

  usernameChangedHandler = (event) => {
    this.setState({username: event.target.value})
  }
  render(){
    return (
      <div className="App">
        <p>Welcome Yves!</p>
        <UserInput changed={this.usernameChangedHandler} currentName={this.state.username}/>
        <UserOutput userName={this.state.username}/>
        <UserOutput userName={this.state.username}/>
        <UserOutput userName="Byiringiro"/>
      </div>
    );
  }
  }


export default App;
